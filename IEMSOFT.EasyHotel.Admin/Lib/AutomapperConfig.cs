﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IEMSOFT.EasyHotel.DAL.Models;
using IEMSOFT.EasyHotel.DAL.Models.Extension;
using IEMSOFT.EasyHotel.Admin.Models;
using IEMSOFT.Foundation;
using IEMSOFT.EasyHotel.Common;
namespace IEMSOFT.EasyHotel.Admin.Lib
{
    public class AutomapperConfig
    {
        public static void Init()
        {
            AutoMapper.Mapper.CreateMap<User, UserModel>();
            AutoMapper.Mapper.CreateMap<GroupHotel, GroupHotelModel>();
            AutoMapper.Mapper.CreateMap<GroupHotelModel, GroupHotel>();
            AutoMapper.Mapper.CreateMap<RoomType, RoomTypeModel>();
            AutoMapper.Mapper.CreateMap<RoomTypeModel, RoomType>();
            AutoMapper.Mapper.CreateMap<TravelAgency, TravelAgencyModel>();
            AutoMapper.Mapper.CreateMap<TravelAgencyModel, TravelAgency>();
            AutoMapper.Mapper.CreateMap<SubHotel, SubHotelModel>();
            AutoMapper.Mapper.CreateMap<SubHotelModel, SubHotel>();
            AutoMapper.Mapper.CreateMap<UserExtension, UserModel>();
            var userFullMap = AutoMapper.Mapper.CreateMap<UserExtension, UserFullModel>();
            userFullMap.ForMember(des => des.Password, source => source.MapFrom(des => des.PassWord.DESDecrypt(CommonConsts.MyDesKey)));
            var userExtMap = AutoMapper.Mapper.CreateMap<UserFullModel, UserExtension>();
            userExtMap.ForMember(des => des.PassWord, source => source.MapFrom(des => des.Password.DESEncrypt(CommonConsts.MyDesKey)));

            var userMap = AutoMapper.Mapper.CreateMap<UserFullModel, User>();
            userMap.ForMember(des => des.PassWord, source => source.MapFrom(des => des.Password.DESEncrypt(CommonConsts.MyDesKey)));
            AutoMapper.Mapper.CreateMap<RoomModel, RoomExtension>();
            AutoMapper.Mapper.CreateMap<RoomExtension, RoomModel>();
            AutoMapper.Mapper.CreateMap<RoomModel, RoomStatusModel>();
            AutoMapper.Mapper.CreateMap<RoomStatusModel, RoomModel>();

            AutoMapper.Mapper.CreateMap<BillModel, Bill>();
            var billModelMap = AutoMapper.Mapper.CreateMap<Bill, BillModel>();
            billModelMap.ForMember(des => des.Created, source => source.MapFrom(des => des.Created.ToLongDateStr()));
            billModelMap.ForMember(des => des.Modified, source => source.MapFrom(des => des.Modified.ToLongDateStr()));
            billModelMap.ForMember(des => des.CheckinDate, source => source.MapFrom(des => des.CheckinDate.ToLongDateStr()));
            billModelMap.ForMember(des => des.CheckoutDate, source => source.MapFrom(des => des.CheckoutDate.ToLongDateStr()));

            AutoMapper.Mapper.CreateMap<BillCreateModel, BillModel>();
            AutoMapper.Mapper.CreateMap<BillModel, BillCreateModel>();

            var billExtMap = AutoMapper.Mapper.CreateMap<BillExtension, BillModel>();
            AutoMapper.Mapper.CreateMap<BillModel, BillExtension>();
            billExtMap.ForMember(des => des.Created, source => source.MapFrom(des => des.Created.ToLongDateStr()));
            billExtMap.ForMember(des => des.Modified, source => source.MapFrom(des => des.Modified.ToLongDateStr()));
            billExtMap.ForMember(des => des.CheckinDate, source => source.MapFrom(des => des.CheckinDate.ToLongDateStr()));
            billExtMap.ForMember(des => des.CheckoutDate, source => source.MapFrom(des => des.CheckoutDate.ToLongDateStr()));

            var billFullMap = AutoMapper.Mapper.CreateMap<BillExtension, BillFullModel>();
            AutoMapper.Mapper.CreateMap<BillFullModel, BillExtension>();
            billFullMap.ForMember(des => des.Created, source => source.MapFrom(des => des.Created.ToLongDateStr()));
            billFullMap.ForMember(des => des.Modified, source => source.MapFrom(des => des.Modified.ToLongDateStr()));
            billFullMap.ForMember(des => des.CheckinDate, source => source.MapFrom(des => des.CheckinDate.ToLongDateStr()));
            billFullMap.ForMember(des => des.CheckoutDate, source => source.MapFrom(des => des.CheckoutDate.ToLongDateStr()));

            AutoMapper.Mapper.CreateMap<BillBasicModel, Bill>();
            AutoMapper.Mapper.CreateMap<Bill, BillBasicModel>();
        }
    }
}