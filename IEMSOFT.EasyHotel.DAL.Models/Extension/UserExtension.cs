﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IEMSOFT.EasyHotel.DAL.Models;
namespace IEMSOFT.EasyHotel.DAL.Models.Extension
{
    public class UserExtension : User
    {
        public string SubHotelName { get; set; }
        public string RoleName { get; set; }
    }
}
