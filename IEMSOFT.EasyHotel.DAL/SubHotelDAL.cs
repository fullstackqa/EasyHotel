﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IEMSOFT.EasyHotel.DAL.Models;
using IEMSOFT.EasyHotel.Common;
namespace IEMSOFT.EasyHotel.DAL
{
    public class SubHotelDAL
    {
        private IDBProvider _dbProvider;
        public SubHotelDAL(IDBProvider dbProvider)
        {
            _dbProvider = dbProvider;
        }

        public List<SubHotel> GetList(int groupHotelId)
        {
            var sql = new StringBuilder();
            sql.Append("select * from SubHotel where GroupHotelId=@0 ");
            sql.Append("order by convert(Name USING gbk) COLLATE gbk_chinese_ci  asc ");
            var ret = _dbProvider.DB.Fetch<SubHotel>(sql.ToString(), groupHotelId);
            return ret;
        }

        public void Save(SubHotel subHotel)
        {
            var result = GetOne(subHotel.SubHotelId);
            if (result == null)
            {
                _dbProvider.DB.Insert(subHotel);
            }
            else
            {
                subHotel.Modified = DateTime.Now;
                subHotel.SubHotelId = result.SubHotelId;
                subHotel.Created = result.Created;
                _dbProvider.DB.Update(subHotel);
            }
        }


        public void Add(SubHotel subHotel)
        {
            _dbProvider.DB.Insert(subHotel);
        }

        public SubHotel GetOne(int groupHotelId,string subHotelName)
        {
            var ret = _dbProvider.DB.FirstOrDefault<SubHotel>("select * from SubHotel where Name=@0 and GroupHotelId=@1", subHotelName, groupHotelId);
            return ret;
        }

        public SubHotel GetOne(int subHotelId)
        {
            var ret = _dbProvider.DB.FirstOrDefault<SubHotel>("select * from SubHotel where SubHotelId=@0", subHotelId);
            return ret;
        }

        public void Remove(int groupHotelId, string subHotelName)
        {
            _dbProvider.DB.Execute("delete from SubHotel where Name=@0 and GroupHotelId=@1", subHotelName, groupHotelId);
        }

        public void Remove(int subHotelId)
        {
            _dbProvider.DB.Delete<SubHotel>(subHotelId);
        }
    }
}
