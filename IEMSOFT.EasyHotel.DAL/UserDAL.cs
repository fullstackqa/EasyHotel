﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IEMSOFT.EasyHotel.Common;
using IEMSOFT.EasyHotel.DAL.Models;
using IEMSOFT.EasyHotel.DAL.Models.Extension;
using IEMSOFT.Foundation;
namespace IEMSOFT.EasyHotel.DAL
{
    public class UserDAL
    {
        private IDBProvider _dbProvider;
        public UserDAL(IDBProvider dbProvider)
        {
            _dbProvider = dbProvider;
        }

        public UserExtension GetOne(int groupHotelId, string userName, int roleId)
        {
            var sql = new StringBuilder();
            sql.Append("select u.*, ");
            sql.Append("       r.Name as RoleName, ");
            sql.Append("       sh.Name as SubHotelName ");
            sql.Append("from user u inner join Role r on u.RoleId=r.RoleId ");
            sql.Append("left join SubHotel sh on u.GroupHotelId=sh.GroupHotelId and u.SubHotelId=sh.SubHotelId ");
            sql.Append("where u.GroupHotelId=@0 and u.RoleId=@1 and u.UserName=@2");

            var ret = _dbProvider.DB.FirstOrDefault<UserExtension>(sql.ToString(), groupHotelId, roleId, userName);
          return ret;
        }

        public User GetOneById(int userId)
        {
            var ret = _dbProvider.DB.FirstOrDefault<User>("select * from User where UserId=@0",
                                               userId);
            return ret;
        }

        public List<UserExtension> GetList(int groupHotelId, int subHotelId, int roleId)
        {
            var sql = new StringBuilder();
            sql.Append("select u.*, ");
            sql.Append("       r.Name as RoleName, ");
            sql.Append("       sh.Name as SubHotelName ");
            sql.Append("from user u inner join Role r on u.RoleId=r.RoleId ");
            sql.Append("left join SubHotel sh on u.GroupHotelId=sh.GroupHotelId and u.SubHotelId=sh.SubHotelId ");
            if (roleId == RoleType.GroupLeader.ToInt())
            {
                sql.Append("where u.GroupHotelId=@0 ");//集团下的所有用户
            }
            else
            {
                sql.AppendFormat("where u.GroupHotelId=@0 and u.subHotelId={0} ",subHotelId);
            }
           var ret= _dbProvider.DB.Fetch<UserExtension>(sql.ToString(), groupHotelId);
           return ret;
            
        }

        public void UpdatePassword(int userId,string newPassword)
        {
            _dbProvider.DB.Execute("update User set PassWord=@0,Modified=@1 where UserId=@2 ", newPassword,DateTime.Now, userId);
        }

        public int Add(User user)
        {
            var ret = _dbProvider.DB.Insert(user);
            return Convert.ToInt32(ret);
        }

        public void Update(User user)
        {
            var existOne = GetOneById(user.UserId);
            user.Created = existOne.Created;
            user.Modified = DateTime.Now;
            _dbProvider.DB.Update(user);
        }

        public void RemoveById(int userId)
        {
            _dbProvider.DB.Delete<User>(userId);
        }

        public void ClearSubHotelBySubHotelId(int groupHotelId, int subHotelId)
        {
            _dbProvider.DB.Execute("update User set SubHotelId=0 where GroupHotelId=@0 and SubHotelId=@1",groupHotelId,subHotelId);
        }
    }
}
