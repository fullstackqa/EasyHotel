﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IEMSOFT.EasyHotel.DAL.Models;
namespace IEMSOFT.EasyHotel.DAL
{
    public class RoomTypeDAL
    {
        private  IDBProvider _dbProvider;
        public RoomTypeDAL(IDBProvider dbProvider)
        {
            _dbProvider = dbProvider;
        }

        public List<RoomType> GetList(int groupHotelId)
        {
            var sql = new StringBuilder();
            sql.Append("select * from RoomType where GroupHotelId=@0");
            var ret = _dbProvider.DB.Fetch<RoomType>(sql.ToString(),groupHotelId);
            return ret;
        }

        public void Save(RoomType roomType)
        {
            var result = GetOne(roomType.RoomTypeId);
            if (result == null)
            {
                _dbProvider.DB.Insert(roomType);
            }
            else
            {
                roomType.Modified = DateTime.Now;
                roomType.Created = result.Created;
                _dbProvider.DB.Update(roomType);
            }
        }

        public RoomType GetOne(int roomTypeId)
        {
            var ret = _dbProvider.DB.FirstOrDefault<RoomType>("select * from RoomType where RoomTypeId=@0 ",
                                                              roomTypeId);
            return ret;
        }
        public RoomType GetOneByName(string roomTypeName, int groupHotelId)
        {
            var ret = _dbProvider.DB.FirstOrDefault<RoomType>("select * from RoomType where Name=@0 and GroupHotelId=@1",
                                                               roomTypeName, groupHotelId);
            return ret;
        }

        public void Remove(List<RoomType> rooms)
        {
            foreach (var item in rooms)
                {
                    Remove(item);
                }         
        }

        public void Remove(RoomType room)
        {
            _dbProvider.DB.Delete(room);
        }
    }
}
